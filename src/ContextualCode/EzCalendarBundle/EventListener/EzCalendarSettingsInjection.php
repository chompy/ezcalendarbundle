<?php

namespace ContextualCode\EzCalendarBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use eZ\Publish\Core\MVC\Legacy\LegacyEvents as eZPublishLegacyEvents;
use eZ\Publish\Core\MVC\Legacy\Event\PreBuildKernelWebHandlerEvent;
use ThinkCreative\LegacyBundle\Services\SettingsInjection;

class EzCalendarSettingsInjection implements EventSubscriberInterface
{

    protected $ezCalendarConfig;

    public function __construct(array $ezCalendarConfig) {
        $this->ezCalendarConfig = $ezCalendarConfig;
    }

    public function onPreBuildKernelWebHandler(PreBuildKernelWebHandlerEvent $event) {
        
        $eventParameters = $event->getParameters();
        $settings = $eventParameters->get("injected-settings");

        if (isset($this->ezCalendarConfig["views"])) {
            $settings["ezoe_attributes.ini/CustomAttribute_contextualcodecalendar_view/Selection"] = array();
            foreach ($this->ezCalendarConfig["views"] as $name => $config) {
                $settings["ezoe_attributes.ini/CustomAttribute_contextualcodecalendar_view/Selection"][$name] = $config["description"];
            }
        }

        $eventParameters->set(
            'injected-settings',
            $settings
        );

    }

    public static function getSubscribedEvents() {
        return array(
            eZPublishLegacyEvents::PRE_BUILD_LEGACY_KERNEL_WEB => array(
                'onPreBuildKernelWebHandler', 128
            ),
        );
    }

}
