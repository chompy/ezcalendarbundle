<?php

namespace ContextualCode\EzCalendarBundle;

use ContextualCode\EzCalendarBundle\DependencyInjection\Compiler\XslRegisterPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeEzCalendarBundle extends Bundle
{

    public function build( ContainerBuilder $container )
    {
        parent::build( $container );
        $container->addCompilerPass( new XslRegisterPass() );
    }

    public function getLegacyExtensionsNames()
    {
        return array( 'contextual_code_calendar' );
    }

}
