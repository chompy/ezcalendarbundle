<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xhtml="http://ez.no/namespaces/ezpublish3/xhtml/"
        xmlns:custom="http://ez.no/namespaces/ezpublish3/custom/"
        xmlns:image="http://ez.no/namespaces/ezpublish3/image/"
        xmlns:php="http://php.net/xsl"
        exclude-result-prefixes="xhtml custom image">
 
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
 
    <!-- Template below will match the following custom tag: -->
    <xsl:template match="custom[@name='contextualcodecalendar']">
        <div class="contextual-code-calendar">
            <xsl:attribute name="data-calendar-view"><xsl:value-of select="@custom:view"/></xsl:attribute>
            <xsl:attribute name="data-calendar-location"><xsl:value-of select="@custom:parentLocation"/></xsl:attribute>
            <xsl:attribute name="data-calendar-filters"><xsl:value-of select="@custom:filters"/></xsl:attribute>
            <xsl:value-of disable-output-escaping="yes" select="@custom:outputHtml"/>
        </div>
    </xsl:template>
</xsl:stylesheet>