<?php /* #?ini charset="utf-8"?

[embed]
AvailableClasses[]=event_rotation
ClassDescription[event_rotation]=Event Rotation

[CustomTagSettings]
AvailableCustomTags[]=contextualcodecalendar
CustomTagsDescription[contextualcodecalendar]=Contextual Code Calendar
IsInline[spacer]=true

[contextualcodecalendar]
CustomAttributes[]=view
CustomAttributes[]=parentLocation
CustomAttributes[]=filters
CustomAttributes[]=limit