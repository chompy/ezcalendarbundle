<?php

use ContextualCode\CalendarBundle\Classes\EventCompiler;

class CalendarCompileType extends eZWorkflowEventType
{
    const WORKFLOW_TYPE_STRING = "calendarcompile";
    public function __construct()
    {
        parent::__construct( CalendarCompileType::WORKFLOW_TYPE_STRING, 'Compile Calendar Events' );
    }
 
    public function execute( $process, $event )
    {
        $container = ezpKernel::instance()->getServiceContainer();
        $storageServiceName = $container->getParameter("contextual_code_calendar.default_storage");
        $storageService = $container->get($storageServiceName);

        EventCompiler::$compilePath = $container->getParameter("contextual_code_calendar.compile_path");
        if (!EventCompiler::$compilePath || !is_dir(EventCompiler::$compilePath)) {
            EventCompiler::$compilePath = $container->getParameter("kernel.cache_dir");
        }

        $eventList = $storageService->getModifiedEvents();
        EventCompiler::compileModified($eventList);
        return eZWorkflowType::STATUS_ACCEPTED;
    }
}
eZWorkflowEventType::registerEventType( CalendarCompileType::WORKFLOW_TYPE_STRING, 'CalendarCompileType' );