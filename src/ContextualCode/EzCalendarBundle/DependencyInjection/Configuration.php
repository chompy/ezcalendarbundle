<?php

namespace ContextualCode\EzCalendarBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('contextual_code_ez_calendar');

        $rootNode
            ->children()
                // @TODO implement, or remove
                // root location that contains all events
                ->integerNode("root_location_id")
                    ->defaultValue(2)
                ->end()

                // content type that contains events
                ->scalarNode("content_type_identifier")
                    ->defaultValue("event")
                ->end()

                // content attributes that contain event data
                ->arrayNode("content_attributes")
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode("title")
                            ->defaultValue("")
                        ->end()
                        ->scalarNode("start_time")
                            ->defaultValue("from_time")
                        ->end()
                        ->scalarNode("end_time")
                            ->defaultValue("to_time")
                        ->end()
                        ->scalarNode("rrule")
                            ->defaultValue("")
                        ->end()
                    ->end()
                ->end()

                // views
                ->arrayNode("views")
                    ->useAttributeAsKey('name')
                    ->prototype("array")
                        ->children()
                            ->scalarNode("class")->end()
                            ->scalarNode("description")->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
