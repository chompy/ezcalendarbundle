<?php

namespace ContextualCode\EzCalendarBundle\DependencyInjection\Compiler;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Copied from 
 * https://github.com/lolautruche/EmbedTagBundle/blob/master/DependencyInjection/Compiler/XslRegisterPass.php
 */
class XslRegisterPass implements CompilerPassInterface
{
    public function process( ContainerBuilder $container )
    {
        if (
            !(
                $container->hasParameter( 'ezsettings.default.fieldtypes.ezxml.custom_xsl' )
                && $container->hasParameter( 'ezpublish.siteaccess.list' )
            )
        )
        {
            return;
        }
        // Adding embed_tag.xsl to all declared siteaccesses.
        foreach ( $container->getParameter( 'ezpublish.siteaccess.list' ) as $sa )
        {
            if ( !$container->hasParameter( "ezsettings.$sa.fieldtypes.ezxml.custom_xsl" ) )
            {
                continue;
            }
            $xslConfig = $container->getParameter( "ezsettings.$sa.fieldtypes.ezxml.custom_xsl" );
            $xslConfig[] = array( 'path' => __DIR__ . '/../../Resources/xsl/contextualcodecalendar.xsl', 'priority' => 20 );
            $container->setParameter( "ezsettings.$sa.fieldtypes.ezxml.custom_xsl", $xslConfig );
        }
    }
}