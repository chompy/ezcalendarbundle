<?php

namespace ContextualCode\EzCalendarBundle\CalendarEventStorage;

use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\Core\Repository\LocationService;
use eZ\Publish\API\Repository\Values\Content\Content;

use ContextualCode\CalendarBundle\Classes\EventCompiler;
use ContextualCode\CalendarBundle\CalendarEventStorage\CalendarEventStorageInterface;
use ContextualCode\CalendarBundle\Classes\CalendarFilter;
use ContextualCode\CalendarBundle\CalendarEvent\CalendarEvent;

/**
 * Pulls calendar events from eZ Publish content
 */
class EzCalendarEventStorage implements CalendarEventStorageInterface
{

    /**
     * @var \eZ\Publish\API\Repository\SearchService
     */
    protected $ezSearchService;

    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $ezLocationService;

    /**
     * @var array
     */
    protected $ezCalendarConfig;


    public function __construct(SearchService $ezSearchService, LocationService $ezLocationService, array $ezCalendarConfig)
    {

        $this->ezSearchService = $ezSearchService;
        $this->ezLocationService = $ezLocationService;
        $this->ezCalendarConfig = $ezCalendarConfig;

    }

    /**
     * @inheritdoc
     */
    protected function getTitle(Content $content)
    {

        $titleField = isset($this->ezCalendarConfig["content_attributes"]["title"]) ? $this->ezCalendarConfig["content_attributes"]["title"] : "";
        return $titleField ? $content->getFieldValue($titleField)->text : $content->contentInfo->name;

    }

    /**
     * @inheritdoc
     */
    protected function getStartTime(Content $content)
    {

        $startTimeField = isset($this->ezCalendarConfig["content_attributes"]["start_time"]) ? $this->ezCalendarConfig["content_attributes"]["start_time"] : "from_time";
        $startTime = $content->getFieldValue($startTimeField)->value;
        $startTime->setTimezone( new \DateTimeZone( date_default_timezone_get() ) );
        return $startTime;

    }

    /**
     * @inheritdoc
     */
    protected function getLength(Content $content)
    {

        $endTimeField = isset($this->ezCalendarConfig["content_attributes"]["end_time"]) ? $this->ezCalendarConfig["content_attributes"]["end_time"] : "to_time";
        $startTime = $this->getStartTime($content);
        return $startTime->diff( $content->getFieldValue($endTimeField)->value );

    }

    /**
     * @inheritdoc
     */
    protected function getRrule(Content $content)
    {

        $rruleField = isset($this->ezCalendarConfig["content_attributes"]["rrule"]) ? $this->ezCalendarConfig["content_attributes"]["rrule"] : "";
        return $rruleField ? $content->getFieldValue($rruleField)->text : "";

    }

    /**
     * @inheritdoc
     */
    protected function getEventFilters(Content $content)
    {

        $location = $this->ezLocationService->loadLocation($content->contentInfo->mainLocationId);
        return  array(
            "parent-location-" . $location->parentLocationId,
            "location-" . $location->id,
            "object-" . $content->id
        );

    }

    /**
     * Fetch events
     * @param boolean $lastModified  If true fetch only modified events since last compilation
     */
    public function getEvents($lastModified = false)
    {

        $contentTypeIdentifier = isset($this->ezCalendarConfig["content_type_identifier"]) ? $this->ezCalendarConfig["content_type_identifier"] : "event";
        $query = new Query();
        $criterion = array(
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\ContentTypeIdentifier($contentTypeIdentifier)
        );      
        if ($lastModified) {
            $lastModifiedDateTime = EventCompiler::getLastModified();
            if ($lastModifiedDateTime) {
                $criterion[] = new Criterion\DateMetadata(
                    Criterion\DateMetadata::MODIFIED,
                    Criterion\Operator::GT,
                    $lastModifiedDateTime->getTimestamp()
                );
            }
        }
        $query->query = new Criterion\LogicalAnd($criterion);
        $results = $this->ezSearchService->findContent($query);

        $events = array();
        foreach ($results->searchHits as $result) {

            $events[] = new CalendarEvent(
                $result->valueObject->id,
                $this->getTitle($result->valueObject),
                $this->getStartTime($result->valueObject),
                $this->getLength($result->valueObject),
                $this->getRrule($result->valueObject),
                $this->getEventFilters($result->valueObject)
            );
        }
        return $events;

    }

    /**
     * @inheritdoc
     */
    public function getAllEvents()
    {

        return $this->getEvents();

    }

    /**
     * @inheritdoc
     */
    public function getModifiedEvents()
    {

        return $this->getEvents(true);

    }



}