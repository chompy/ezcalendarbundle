<?php

namespace ContextualCode\EzCalendarBundle\CalendarEventStorage;

use eZ\Publish\API\Repository\Values\Content\Content;
use ContextualCode\CalendarBundle\CalendarEventStorage\CalendarEventStorageInterface;

/**
 * Interface which defines method of eZ event retrival.
 */
interface EzCalendarEventStorageInterface extends CalendarEventStorageInterface
{

    /**
     * Get event title from content containing event data
     * @param \eZ\Publish\API\Repository\Values\Content\Content
     */
    protected function getTitle(Content $content);

    /**
     * Get event start time from content containing event data
     * @param \eZ\Publish\API\Repository\Values\Content\Content
     */
    protected function getStartTime(Content $content);

    /**
     * Get event length from content containing event data
     * @param \eZ\Publish\API\Repository\Values\Content\Content
     */
    protected function getLength(Content $content);

    /**
     * Get event rrule from content containing event data
     * @param \eZ\Publish\API\Repository\Values\Content\Content
     */
    protected function getRrule(Content $content);

    /**
     * Get list of filters to apply for given event content
     * @param eZ\Publish\API\Repository\Values\Content\Content $content
     * @return array
     */
    public function getEventFilters(Content $content);
    
}