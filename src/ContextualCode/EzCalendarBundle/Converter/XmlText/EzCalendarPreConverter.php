<?php

namespace ContextualCode\EzCalendarBundle\Converter\XmlText;

use DOMDocument;
use eZ\Publish\Core\FieldType\XmlText\Converter;
use DOMXPath;

use Symfony\Component\HttpFoundation\Request;
use ContextualCode\CalendarBundle\Controller\CalendarController;

class EzCalendarPreConverter implements Converter
{

    /**
     * Name of eZ custom tag
     * @string
     */
    const CUSTOMTAG_NAME = "contextualcodecalendar";

    /**
     * @string
     */
    const CUSTOMTAG_NAMESPACE = 'http://ez.no/namespaces/ezpublish3/custom/';

    /**
     * @var ContextualCode\CalendarBundle\Controller\CalendarController
     */
    protected $calendarController;

    /**
     * @var array
     */
    protected $config;

    public function __construct(CalendarController $calendarController, array $config)
    {
        $this->calendarController = $calendarController;
        $this->config = $config;
    }

    public function convert( DOMDocument $xmlDoc )
    {

        $request = Request::createFromGlobals();
        $xpath = new DOMXPath( $xmlDoc );
        $tags = $xpath->query( "//custom[@name='".self::CUSTOMTAG_NAME."']" );
        foreach ($tags as $tag) {
            
            $view = $tag->getAttributeNS( self::CUSTOMTAG_NAMESPACE, 'view' );
            if (isset($this->config["views"][$view]["class"])) {
                $view = $this->config["views"][$view]["class"];
            }

            $filters = explode(",", trim($tag->getAttributeNS( self::CUSTOMTAG_NAMESPACE, 'filters' )));

            $parentLocation = trim( str_replace( "eznode://", "", $tag->getAttributeNS( self::CUSTOMTAG_NAMESPACE, 'parentLocation' ) ) );
            if ($parentLocation && ctype_digit($parentLocation)) {
                $filters[] = sprintf("parent-location-%d", $parentLocation);
            }

            $limit = trim( $tag->getAttributeNS( self::CUSTOMTAG_NAMESPACE, 'limit' ) );
            if (!ctype_digit($limit)) {
                $limit = -1;
            }

            $output = $this->calendarController->viewCalendarAction(
                $request, 
                $view,
                null,
                $filters,
                $limit
            );

            $tag->setAttributeNS( self::CUSTOMTAG_NAMESPACE, 'outputHtml', $output->getContent() );
        }
    }
}
 