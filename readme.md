# Contextual Code eZ Calendar Bundle

### 1) Setup

* Setup the Contextual Code Calendar Bundle (https://bitbucket.org/chompy/calendarbundle)
* Add ```ContextualCode\EzCalendarBundle\ContextualCodeEzCalendarBundle``` to the bundles list EzpublishKernel.php.
* Define configuration for calendar in config.yml...

```
contextual_code_ez_calendar:
    calendars:
        default:
            event_content_type: event
            title_field: title
            start_time_field: from_time
            end_time_field: to_time
            rrule_field: rrule
```
You can define multiple configurations if you site has multiple calendar event types.

### 2) Usage

Example code...

```
use ContextualCode\CalendarBundle\Classes\CalendarFilter;
use ContextualCode\CalendarBundle\CalendarView;

$categoryFilter = new CalendarFilter(CalendarFilter::LOGIC_AND);
$categoryFilter->addCondition(CalendarFilter::LOGIC_EQ, "parent_location_id", 39572);       

$view = new CalendarView\MonthCalendarView(
    new \DateTime("01/01/2016 12:00 AM"),
    $categoryFilter
);

$cal = $this->get("contextual_code_ez_calendar");
$events = $cal->getEventsInView($view);
foreach ($events as $event) {
    $dates = $cal->getEventDatesInView($event, $view);
}
```